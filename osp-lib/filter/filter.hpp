//
// Created by dsengupt on 9/18/18.
//

#ifndef OSP_LIBS_FILfloatER_H
#define OSP_LIBS_FILfloatER_H

#include <memory>
#include <mutex>
#include <circular_buffer.hpp>

class filter {

public:

    explicit filter(float* taps, size_t tap_size, circular_buffer* cir_buf, size_t max_buf_size){
        tap_ = new float[tap_size];
        size_ = tap_size;
        if(cir_buf != nullptr){
            cir_buf_ = cir_buf;
        }
        else{
            cir_buf_ = new circular_buffer(tap_size+max_buf_size, 0);
        }
        set_taps(taps, size_);
    };
    ~filter() = default;
    int set_taps(const float* taps, size_t buf_size){
        mutex_.lock();
        if(buf_size != this->size_){
            mutex_.unlock();
            return -1;
        }
        for(size_t i = 0; i < buf_size; i++){
            this->tap_[i] = taps[i];
        }
        mutex_.unlock();
        return 0;
    };
    int get_taps(float* taps, size_t buf_size){
        mutex_.lock();
        if(buf_size != this->size_){
            mutex_.unlock();
            return -1;
        }
        for(size_t i = 0; i < buf_size; i++){
            taps[i] = this->tap_[i];
        }
        mutex_.unlock();
        return 0;
    };
    void cirfir(float* data_out, size_t num_samp){
        mutex_.lock();
        cir_buf_->mutex_.lock();
        float temp_data_out;
        auto mask = (int)cir_buf_->mask_;
        auto head = (int)cir_buf_->head_;
        auto size = (int)size_;
        float * data = cir_buf_->buf_;
        size_t index = (head + mask - size - num_samp + 2) & mask;
//        std::cout << "Head = " << head << " mask = " << mask << " index = " << index << " samp_num = " << num_samp << std::endl;
        for(size_t i = 0; i < num_samp; i++){
            temp_data_out = 0;
            for(int j = 0; j < size; j++){
                temp_data_out += data[(i + j + index) & mask] * tap_[j];
            }
            data_out[i] = temp_data_out;
        }
        cir_buf_->mutex_.unlock();
        mutex_.unlock();
    };
    void cirfir(float* data_in, float* data_out, size_t num_samp){
        cir_buf_->put(data_in, num_samp);
        this->cirfir(data_out, num_samp);
    }
    size_t get_size(){
        return size_;
    };

private:
    float* tap_;
    size_t size_;
    circular_buffer* cir_buf_;
    std::mutex mutex_;
};

#endif //OSP_LIBS_FILfloatER_H
