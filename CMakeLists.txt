cmake_minimum_required(VERSION 3.8)
project(osp_clion_cxx)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -pthread -O3")

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -O3 -Wall -Wextra")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pthread")

IF(APPLE)
    FIND_LIBRARY(CORE_SERVICES CoreServices)
    FIND_LIBRARY(CORE_FOUNDATION CoreFoundation )
    FIND_LIBRARY(AUDIO_UNIT AudioUnit )
    FIND_LIBRARY(AUDIO_TOOLBOX AudioToolbox )
    FIND_LIBRARY(CORE_AUDIO CoreAudio)
    MARK_AS_ADVANCED (CORE_SERVICES
            CORE_FOUNDATION
            AUDIO_UNIT
            AUDIO_TOOLBOX
            CORE_AUDIO)
    SET(EXTRA_LIBS ${CORE_SERVICES} ${CORE_FOUNDATION} ${AUDIO_UNIT} ${AUDIO_TOOLBOX} ${CORE_AUDIO})
ENDIF ()

IF(UNIX AND NOT APPLE)
    find_package( ALSA )
ENDIF()

include_directories(parsers osp-lib/resample osp-lib/filter osp-lib/circular_buffer osp-lib/array_utilities
        osp-lib/subband thread_pool osp_example portaudio_wrapper .)

add_executable(osp_clion_cxx main.cpp parsers/osp_parser.hpp osp_param.h
        filter_coef.h portaudio_wrapper/portaudio_wrapper.cpp portaudio_wrapper/portaudio_wrapper.h
        osp_example/osp_process.hpp osp-lib/resample/resample.hpp osp-lib/circular_buffer/circular_buffer.hpp
        osp-lib/filter/filter.hpp osp-lib/array_utilities/array_utilities.cpp osp-lib/array_utilities/array_utilities.h
        osp-lib/subband/noise_mangement.hpp osp-lib/subband/peak_detect.hpp osp-lib/subband/wdrc.hpp
        thread_pool/ThreadPool.h thread_pool/SafeQueue.h control_param.hpp osp-lib/osp_tcp/osp_tcp.cpp osp-lib/osp_tcp/osp_tcp.h osp-lib/osp_tcp/constants.h)

IF(APPLE)
    target_link_libraries(osp_clion_cxx /usr/local/lib/libportaudio.a ${EXTRA_LIBS} m)
ENDIF()
IF(UNIX AND NOT APPLE)
    target_link_libraries(osp_clion_cxx /usr/local/lib/libportaudio.a ${EXTRA_LIBS} m rt asound)
ENDIF()
