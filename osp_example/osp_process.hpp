//
// Created by Dhiman Sengupta on 10/1/18.
//

#ifndef OSP_CLION_CXX_OSP_PROCESS_H
#define OSP_CLION_CXX_OSP_PROCESS_H

#include <resample.hpp>
#include <iostream>
#include <unistd.h>
#include <thread>
#include "circular_buffer.hpp"
#include "osp_param.h"
#include "48_32_filter.h"
#include "32_48_filter.h"
#include "filter_coef.h"
#include "array_utilities.h"
#include "noise_mangement.hpp"
#include "peak_detect.hpp"
#include "wdrc.hpp"

class osp_process{

public:
    explicit osp_process(float samp_freq, size_t max_buffer_in, osp_user_data *user_data){
        for(int i = 0; i < NUM_CHANNEL; i++) {
            user_data[i].samp_freq = samp_freq;
            en_ha[i] = user_data[i].enable_ha;

            attenuation[i] = powf(10.0f, user_data[i].attenuation_db / 20.0f);

            if (samp_freq == 48000) {
                max_dwn_buf = max_buffer_in * 2;
                max_dwn_buf = max_dwn_buf / 3;
                down_sample[i] = new resample(filter_48_32, FILTER_48_32_SIZE, max_buffer_in, 2, 3);
                up_sample[i] = new resample(filter_32_48, FILTER_32_48_SIZE, max_dwn_buf, 3, 2);
            } else {
                std::cout << "Error setting up osp process" << std::endl;
                return;
            }
            e_n[i] = new circular_buffer(max_dwn_buf + BAND_FILT_LEN, 0.0f);
            for (int j = 0; j < NUM_BANDS; j++) {
                filters[i][j] = new filter(subband_filter[j], BAND_FILT_LEN, e_n[i], max_buffer_in);
                noiseMangement[i][j] = new noise_mangement(user_data[i].noise_estimation_type, user_data[i].spectral_type,
                                                 user_data[i].spectral_subtraction, samp_freq);
                peakDetect[i][j] = new peak_detect(samp_freq, user_data[i].attack[j], user_data[i].release[j]);
                wdrcs[i][j] = new wdrc(user_data[i].g50[j], user_data[i].g80[j], user_data[i].knee_low[j], user_data[i].mpo);
            }
        }


    };
    ~osp_process(){

        for(int i = 0; i < NUM_CHANNEL; i++){
            delete up_sample[i];
            delete down_sample[i];
            delete e_n[i];
            for(int j = 0; j < NUM_BANDS; j++){
                delete filters[i][j];
                delete noiseMangement[i][j];
                delete peakDetect[i][j];
                delete wdrcs[i][j];
            }
        }
        
    };
    void process(float** input, float** output, size_t buf_size){
        param_mutex_.lock();
//        auto start = std::chrono::high_resolution_clock::now();
        for(int j = 0; j < NUM_CHANNEL; j++) {
            array_multiply_const(input[j], attenuation[j], buf_size);
        }
//            std::thread left(&osp_process::process_channels, this, input[0], output[0], 0, buf_size);
        this->process_channels(input[0], output[0], 0, buf_size);
        this->process_channels(input[1], output[1], 1, buf_size);
//            left.join();

//
//


//        auto elapsed = std::chrono::high_resolution_clock::now() - start;
//        std::cout << std::chrono::duration_cast<std::chrono::microseconds>(elapsed).count() << std::endl;
        param_mutex_.unlock();
        
    };
    void set_params(osp_user_data *user_data){
        param_mutex_.lock();
        for(int j = 0; j < NUM_CHANNEL; j++) {
            attenuation[j] = powf(10.0f, user_data[j].attenuation_db / 20.0f);
            en_ha[j] = user_data[j].enable_ha;
            for (int i = 0; i < NUM_BANDS; i++) {
                noiseMangement[j][i]->set_param(user_data[j].noise_estimation_type, user_data[j].spectral_type, user_data[j].spectral_subtraction);
                peakDetect[j][i]->set_param(user_data[j].attack[i], user_data[j].release[i]);
                wdrcs[j][i]->set_param(user_data[j].g50[i], user_data[j].g80[i], user_data[j].knee_low[i], user_data[j].mpo);
            }
        }
        param_mutex_.unlock();
    };
    void get_params(osp_user_data *user_data){
        param_mutex_.lock();
        for(int j = 0; j < NUM_CHANNEL; j++) {
            user_data[j].attenuation_db = log10f(attenuation[j]) * 20.0f;
            user_data[j].enable_ha = en_ha[j];
            for (int i = 0; i < NUM_BANDS; i++) {
                noiseMangement[j][i]->get_param(user_data[j].noise_estimation_type, user_data[j].spectral_type, user_data[j].spectral_subtraction);
                peakDetect[j][i]->get_param(user_data[j].attack[i], user_data[j].release[i]);
                wdrcs[j][i]->get_param(user_data[j].g50[i], user_data[j].g80[i], user_data[j].knee_low[i], user_data[j].mpo);
            }
        }
        param_mutex_.unlock();
    };
    void process_channels(float *in, float *out, int channel, size_t buf_size){

        if(en_ha[channel]) {
            float dwn_samp_data[max_dwn_buf];
            float sub_data[max_dwn_buf];
            float nm_data[max_dwn_buf];
            float pdb_data[max_dwn_buf];
            float wdrc_data[max_dwn_buf];
            float s_n_data[max_dwn_buf];
            size_t out_size;

            for (size_t i = 0; i < max_dwn_buf; i++) {
                s_n_data[i] = 0;
            }


            down_sample[channel]->resamp(in, buf_size, dwn_samp_data, &out_size);
            e_n[channel]->put(dwn_samp_data, out_size);
            for (int j = 0; j < NUM_BANDS; j++) {
                filters[channel][j]->cirfir(sub_data, out_size);
                noiseMangement[channel][j]->speech_enhancement(sub_data, out_size, nm_data);
                peakDetect[channel][j]->get_spl(nm_data, out_size, pdb_data);
                wdrcs[channel][j]->process(nm_data, pdb_data, out_size, wdrc_data);
                array_add_array(s_n_data, wdrc_data, out_size);
            }
            up_sample[channel]->resamp(s_n_data, out_size, out, &out_size);
        }
        else{
            std::memcpy(out, in, sizeof(float)*buf_size);
        }

    };

private:

    std::mutex param_mutex_;
    float attenuation[NUM_CHANNEL];
    int en_ha[NUM_CHANNEL];
    resample* up_sample[NUM_CHANNEL];
    resample* down_sample[NUM_CHANNEL];
    circular_buffer *e_n[NUM_CHANNEL];
    filter *filters[NUM_CHANNEL][NUM_BANDS];
    noise_mangement *noiseMangement[NUM_CHANNEL][NUM_BANDS];
    peak_detect *peakDetect[NUM_CHANNEL][NUM_BANDS];
    wdrc *wdrcs[NUM_CHANNEL][NUM_BANDS];
    size_t max_dwn_buf;



};

#endif //OSP_CLION_CXX_OSP_PROCESS_H
