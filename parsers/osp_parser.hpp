//
// Created by dsengupt on 9/24/18.
//

#ifndef OSP_CLION_C_OSP_PARSER_H
#define OSP_CLION_C_OSP_PARSER_H

#include "cxxopts.hpp"
#include "osp_param.h"
#include "control_param.hpp"

class osp_parser{

public:
    explicit osp_parser(){
        options = new cxxopts::Options("OSP CLION CXX", "Welcome to the Open Speech Platform");
        options
                ->positional_help("[optional args]")
                .show_positional_help();
        options
                ->add_options()
                        ("e,en_ha", "Enable hearing aid algorithm: 0 - Disable; 1 - Enable", cxxopts::value<int>()->default_value("1"))
                        ("a,afc", "Automatic Feedback Cancellation: 0 - AFC off; 1 - FXLMS; 2 - PMLMS; 3 - SLMS", cxxopts::value<int>()->default_value("0"))
                        ("r,en_rear_mic", "Enable rear microphones: 0 - Disable; 1 - Enable", cxxopts::value<int>()->default_value("0"))
                        ("g50", "Set the gain values at 50 dB SPL input level", cxxopts::value<std::string>())
                        ("g80", "Set the gain values at 80 dB SPL input level", cxxopts::value<std::string>())
                        ("knee_low", "Set the lower knee points for the bands", cxxopts::value<std::string>())
                        ("knee_high", "Set the upper knee points for the bands", cxxopts::value<std::string>())
                        ("attack", "Set the attack time for WDRC for the bands", cxxopts::value<std::string>())
                        ("release", "Set the release time for WDRC for the bands", cxxopts::value<std::string>())
                        ("mpo", "Set the mpo limit for all the bands", cxxopts::value<float>()->default_value("100"))
                        ("attenuation", "Set the attenuation in dB", cxxopts::value<float>()->default_value("0"))
                        ("samp_freq", "Set the sampling frequency for the mic and reciever", cxxopts::value<float>()->default_value("48000"))
                        ("input_device", "Please indicate which device you want to use for input", cxxopts::value<int>())
                        ("output_device", "Please indicate which device you want to use for output", cxxopts::value<int>())
                        ("q,quit", "Quit OSP")
                        ("h,help", "Prints out the help");
    };
    ~osp_parser() = default;
    void parse(int argc, char* argv[], osp_user_data *user_data, controls *main_controls){

        osp_user_data* left_user_data = &user_data[0];
        osp_user_data* right_user_data = &user_data[1];

    
        try {
            auto result = options->parse(argc, argv);
            if (result.count("help")) {
                std::cout << options->help({"", "Group"}) << std::endl;
            }
            if(result.count("en_ha")){
                left_user_data->enable_ha = result["en_ha"].as<int>();
                right_user_data->enable_ha = left_user_data->enable_ha;
            }
            if(result.count("afc")){
                left_user_data->afc = result["afc"].as<int>();
                right_user_data->afc = left_user_data->afc;
            }
            if(result.count("g50")){
    
                auto& g50 = result["g50"].as<std::string>();
                std::istringstream iss(g50);
                std::vector<std::string> g50_arr((std::istream_iterator<WordDelimitedBy<','>>(iss)),
                                                 std::istream_iterator<WordDelimitedBy<','>>());
                if(g50_arr.size() == NUM_BANDS) {
                    for (int j = 0; j < NUM_BANDS; j++) {
                        left_user_data->g50[j] = stof(g50_arr[j]);
                        right_user_data->g50[j] = left_user_data->g50[j];
                    }
                }
                else{
                    std::cout << "For g50 please have " << NUM_BANDS << " bands worth of data."  << std::endl;
                }
            }
            if(result.count("g80")){
    
                auto& g80 = result["g80"].as<std::string>();
                std::istringstream iss(g80);
                std::vector<std::string> g80_arr((std::istream_iterator<WordDelimitedBy<','>>(iss)),
                                                 std::istream_iterator<WordDelimitedBy<','>>());
                if(g80_arr.size() == NUM_BANDS) {
                    for (int j = 0; j < NUM_BANDS; j++) {
                        left_user_data->g80[j] = stof(g80_arr[j]);
                        right_user_data->g80[j] = left_user_data->g80[j];
                    }
                }
                else{
                    std::cout << "For g80 please have " << NUM_BANDS << " bands worth of data."  << std::endl;
                }
            }
            if(result.count("knee_low")){
    
                auto& knee_low = result["knee_low"].as<std::string>();
                std::istringstream iss(knee_low);
                std::vector<std::string> knee_low_arr((std::istream_iterator<WordDelimitedBy<','>>(iss)),
                                                 std::istream_iterator<WordDelimitedBy<','>>());
                if(knee_low_arr.size() == NUM_BANDS) {
                    for (int j = 0; j < NUM_BANDS; j++) {
                        left_user_data->knee_low[j] = stof(knee_low_arr[j]);
                        right_user_data->knee_low[j] = left_user_data->knee_low[j];
                    }
                }
                else{
                    std::cout << "For knee_low please have " << NUM_BANDS << " bands worth of data."  << std::endl;
                }
            }
            if(result.count("knee_high")){
    
                auto& knee_high = result["knee_high"].as<std::string>();
                std::istringstream iss(knee_high);
                std::vector<std::string> knee_high_arr((std::istream_iterator<WordDelimitedBy<','>>(iss)),
                                                 std::istream_iterator<WordDelimitedBy<','>>());
                if(knee_high_arr.size() == NUM_BANDS) {
                    for (int j = 0; j < NUM_BANDS; j++) {
                        left_user_data->knee_high[j] = stof(knee_high_arr[j]);
                        right_user_data->knee_high[j] = left_user_data->knee_high[j];
                    }
                }
                else{
                    std::cout << "For knee_high please have " << NUM_BANDS << " bands worth of data."  << std::endl;
                }
            }
            if(result.count("attack")){
    
                auto& attack = result["attack"].as<std::string>();
                std::istringstream iss(attack);
                std::vector<std::string> attack_arr((std::istream_iterator<WordDelimitedBy<','>>(iss)),
                                                 std::istream_iterator<WordDelimitedBy<','>>());
                if(attack_arr.size() == NUM_BANDS) {
                    for (int j = 0; j < NUM_BANDS; j++) {
                        left_user_data->attack[j] = stof(attack_arr[j]);
                        right_user_data->attack[j] = left_user_data->attack[j];
                    }
                }
                else{
                    std::cout << "For attack please have " << NUM_BANDS << " bands worth of data."  << std::endl;
                }
            }
            if(result.count("release")){
    
                auto& release = result["release"].as<std::string>();
                std::istringstream iss(release);
                std::vector<std::string> release_arr((std::istream_iterator<WordDelimitedBy<','>>(iss)),
                                                 std::istream_iterator<WordDelimitedBy<','>>());
                if(release_arr.size() == NUM_BANDS) {
                    for (int j = 0; j < NUM_BANDS; j++) {
                        left_user_data->release[j] = stof(release_arr[j]);
                        right_user_data->release[j] = left_user_data->release[j];
                    }
                }
                else{
                    std::cout << "For release please have " << NUM_BANDS << " bands worth of data."  << std::endl;
                }
            }
            if(result.count("mpo")){
                left_user_data->mpo = result["mpo"].as<float>();
                right_user_data->mpo = result["mpo"].as<float>();
            }
            if(result.count("attenuation")){
                left_user_data->attenuation_db = result["attenuation"].as<float>();
                right_user_data->attenuation_db = result["attenuation"].as<float>();
            }
            if(result.count("quit")){
                main_controls->endloop = true;
            }
            if(result.count("input_device")){
                main_controls->input_device = result["input_device"].as<int>();
            }
            if(result.count("output_device")){
                main_controls->output_device = result["output_device"].as<int>();
            }
    
            std::cout << "Arguments remain = " << argc << std::endl;
    
        } catch (const cxxopts::OptionException &e) {
            std::cout << "error parsing options: " << e.what() << std::endl;
        }
    };

private:
    cxxopts::Options *options;


};


#endif //OSP_CLION_C_OSP_PARSER_H
