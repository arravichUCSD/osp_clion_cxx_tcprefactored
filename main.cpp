#include <iostream>
#include <unistd.h>
#include "osp_parser.hpp"
#include "osp_param.h"
#include <string>
#include "osp_process.hpp"
#include "portaudio_wrapper.h"
#include "control_param.hpp"
#include <osp-lib/osp_tcp/osp_tcp.h>
#include </usr/local/include/mysql/mysql.h>
#include <pthread.h>




static int patestCallback( const void *inputBuffer, void *outputBuffer,
                           unsigned long framesPerBuffer,
                           const PaStreamCallbackTimeInfo* timeInfo,
                           PaStreamCallbackFlags statusFlags,
                           void *userData ) {
    (void) timeInfo;
    (void) &statusFlags;
    auto masterHA = (osp_process *) userData;
    auto input = (float *) inputBuffer;
    auto output = (float*) outputBuffer;
    auto in = new float*[NUM_CHANNEL];
    auto out = new float*[NUM_CHANNEL];
    for(int i = 0; i < NUM_CHANNEL; i++){
        in[i] = new float[framesPerBuffer];
        out[i] = new float[framesPerBuffer];
    }
    for(size_t i = 0; i < framesPerBuffer; i++){
        in[0][i] = input[2*i];
        in[1][i] = input[2*i +1];
    }
    masterHA->process(in, out, framesPerBuffer);
    for(size_t i = 0; i < framesPerBuffer; i++){
        output[2*i] = out[0][i];
        output[2*i +1] = out[1][i];
    }
    for(int i = 0; i < NUM_CHANNEL; i++){
        delete in[i];
        delete out[i];
    }
    delete[] in;
    delete[] out;
    return 0;

}

osp_parser *parser;
osp_user_data user_data[NUM_CHANNEL];
controls main_controls;
Osp_tcp osp_tcp;

static void* run_pa_tcp(void*)
{
    ssize_t ret;
    //int i;
    //FILE *log_file = NULL;
    int tcp_running = 1;
    char message[512];
    //char JSON_STRING_NAL[512];
    unsigned int num_fails = 0;

    Osp_tcp osp_tcp;
    int req[2];
    int action, id;


    //osp2_user_data *osp_tmp_data;
    //osp2_user_data *osp_tmp_nl2;
    /*
     struct Client *client;
     struct PTA *pta;
     struct HAParams *ha;

     ha->compSpeed = 1; // 0 - very slow; 1 - very fast; 2 - dual
     ha->level = 65;
     ha->numAids = 1; // 0 - unilateral; 1 - bilateral
     ha->mic = 1; // 0 - Undisturbed field; 1 - Head Surface
     ha->direction = 0; // 0 - 0; 1 - 45
     ha->limiting = 2; // 0 - off; 1 - wideband; 2 - multichannel
     ha->channels = 6; // 1 - 18; For the OSP, it is 6.
     ha->WBCT = 52; //  52 default NL2 value; 20 - 100
     ha->bandwidth = 1; // 0 - broadband; 1 - narowband;
     // Used to be 0 in nl2port. Changing to 1 for nl2app.
     // #21. ct is needed for knee_low in OSP
     ha->selection = 0; // 0 - REIG; 1 - REAG; 2 - 2ccCoupler; 3 - EarSimulator
     ha->haType = 3; // 0 - CIC; 1 - ITC; 2 - ITE; 3 - BTE
     ha->defValues = 0; // 0 - use predicted data; 1 - use client data
     */

    osp2_user_data *osp_tmp_data;
    osp_tmp_data = (osp2_user_data *)malloc(sizeof(struct osp2_user_data_t));
    //memcpy(osp_tmp_data, osp_data, sizeof(osp2_user_data));

    //printf("REAR MICS = %d\n", osp_tmp_data->rear_mics);

    // init TCP connection
    printf("Initializing TCP connection.\n");
    if ((osp_tcp = osp_tcp_init(UI_PORT)) == NULL) {
        printf("There was an error initializing OSP TCP layer\n");
        return (void *)-1;
    }

    /*
    if (init_pa(&pa_data, samp_rate, frames_per_buffer) < 0) {
        printf("Error initializing port audio stuff\n");
        return -1;
    }
    */

    printf("Entering infinite for loop\n");
    for (;;) {
        fprintf(stderr, "\nWaiting for a connection from the client...\n");
        if (osp_tcp_connect(osp_tcp) < 0) {
            printf("Error on getting server connection\n");
            if (num_fails < 5) {
                printf("Num fails: %d",num_fails);
                num_fails++;
                continue;
            }

            fprintf(stderr, "Failed to set up TCP server 5 times, aborting\n");
            break;
        }

        printf("\nDone!!!\nClient connected\n\n");
        //pa_data.aux_data.underruns = 0;
        tcp_running = 1;

        while (tcp_running == 1) {
#if 0
            ret = read_tcp_server_stream(ui_connfd, &req, sizeof(req));
            if (ret < 0) {
                perror("Failed to read connection, resetting\n");
                break;
            } else if (ret == 0){
                printf("Client side closed, resetting connection\n");
                break;
            }
#endif

            if ((osp_tcp_read_req(osp_tcp)) < 0) {
                printf("\nFailed to read connection, resetting\n");
                tcp_running = 0;
                break;
            }

            action = req[0];
            id = req[1];

            //            else if (*req == OSP_WRONG_VERSION) {
            //                printf("\nWrong version OSP packet from client\n");
            //            }

            //            fprintf(stderr, "Got req %d\n", req);

            switch (action) {
                case OSP_REQ_UPDATE_VALUES:
                    printf("\nRequest to update OSP values\n");
#if 0
                    ret = read_tcp_server_stream(ui_connfd, (char *)osp_tmp_data, sizeof(osp_user_data));
#endif
                    if ((ret = osp_tcp_read_values(osp_tcp, osp_tmp_data, id)) < 0) {

                        //size of osp_user_data is 168, we need only first 160 bytes to be updated.
                        printf("Failed to read values packet from client\n");
                        tcp_running = 0;
                        break;
                    } else if (ret == 0) {
                        tcp_running = 0;
                        printf("There was a disconnect trying to read \"values\" packet from client\n");
                        break;
                    }

#if 0
                if (file_logger_log_osp_data(log_file, osp_tmp_data) < 0) {
                        fprintf(stderr, "Failed to log values to log file\n");
                    }
#endif
                    // updating osp parameters:
                    /*
                    if(update_osp_data_parameters(osp_tmp_data, osp_data)){

                    }else{
                        printf("Parameters not updated. \n");
                    }*/


                    break;

                case 9:
                    printf("Write to MySQL database\n");
                    #if 0 //Arun
                    mysql_write_function(*osp_data,id);
                    #endif //Arun
                    printf("OSP Data written to the database\n");
                    break;
                    /*
                              case 10:

                                  get_nal_parameters(id, *client, *pta);
                                  osp_nl2_prescription(client, pta, ha, osp_tmp_nl2);
                                  mysql_write_function(*osp_tmp_nl2,id+1);
                                  break;
                      */
                case OSP_REQ_GET_UNDERRUNS:
                    printf("\nReceived underrun request packet\n");
                    //fprintf(stderr, "Sending underruns %d\n", pa_data.aux_data.underruns);
#if 0 //Arun
                    if (osp_tcp_send_underruns(osp_tcp, pa_data.aux_data.underruns) < 0) {
                        printf("Error sending underrun packet to client\n");
                        tcp_running = 0;
                    }
#endif //Arun
                    break;
                case OSP_REQ_GET_NUM_BANDS:
                    printf("\nRequest to report number of bands\n");
#if 0 //Arun
                    if (osp_tcp_send_num_bands(osp_tcp, osp_get_num_bands()) < 0) {
                        printf("Failed to notify client to num of bands\n");
                        tcp_running = 0;
                    }
#endif //Arun
                    break;
                case OSP_REQ_USER_ID:
                    // Open log with given username
                    osp_tcp_read_user_packet(osp_tcp, message, sizeof(message));

#if 0 //Arun
                    if ((log_file = file_logger_init(message)) == NULL) {
                        fprintf(stderr, "Failed to open Log File\n");
                        tcp_running = 0;
                    }

                    printf("\nGot userID packet from host: %s\n", message);
#endif //Arun
                    break;
                case OSP_REQ_USER_ACTION:
                    // Log the string straight to the log
                    osp_tcp_read_user_packet(osp_tcp, message, sizeof(message));
#if 0 //Arun
                    file_logger_log_message(log_file, message);
                    printf("\nGot userAction packet from host\n");
#endif //Arun
                    break;
                case OSP_DISCONNECT:
#if 0 //Arun
                    file_logger_close(log_file);
                    tcp_running = 0;
#endif //Arun
                    break;
                default:
                    printf("\nDid not recognize packet received\n");
                    break;
            }
        }
    }

    // close TCP stuff
    osp_tcp_close(osp_tcp);


    return (void *)0;
}

int main(int argc, char* argv[]){
    parser = new osp_parser();
    pthread_t msg;
    parser->parse(argc, argv, user_data, &main_controls);
    auto masterHA = new osp_process(48000, 48, user_data);
    portaudio_wrapper *audio;
    if(main_controls.input_device == -1) {
       audio = new  portaudio_wrapper(2, 2, patestCallback, (void *) masterHA, 48000, 48);
    }
    else{
        audio = new  portaudio_wrapper(main_controls.input_device, 2, main_controls.output_device, 2, patestCallback,
                (void *) masterHA, 48000, 48);
    }
    audio->start_stream();
    if( pthread_create(&msg, NULL, run_pa_tcp, NULL) == 0)
    {
        printf("TCP Thread created and running");
    }
    while(!main_controls.endloop){

        std::string command_line_in;
        std::getline(std::cin, command_line_in);
        std::istringstream iss(command_line_in);
        std::vector<std::string> command_line_array((std::istream_iterator<WordDelimitedBy<' '>>(iss)),
                                             std::istream_iterator<WordDelimitedBy<' '>>());
        command_line_array.insert(command_line_array.begin(), "Temp");
        std::vector<char *> cstrings;
        for(auto& string : command_line_array)
            cstrings.push_back(&string.front());


        parser->parse((int)cstrings.size(), cstrings.data(), user_data, &main_controls);

        masterHA->set_params(user_data);

    }
    audio->stop_stream();
    delete audio;
    delete masterHA;
    delete parser;


    return 0;

}
