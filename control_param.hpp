//
// Created by Dhiman Sengupta on 10/9/18.
//

#ifndef OSP_CLION_CXX_CONTROL_PARAM_HPP
#define OSP_CLION_CXX_CONTROL_PARAM_HPP

typedef struct control_t{
    int input_device = -1;
    int output_device = -1;
    bool endloop = false;
}controls;

#endif //OSP_CLION_CXX_CONTROL_PARAM_HPP
