//
// Created by dsengupt on 9/24/18.
//

#ifndef OSP_CLION_C_OSP_STRUCTURE_H
#define OSP_CLION_C_OSP_STRUCTURE_H

#include "filter_coef.h"
/**
 * @brief general data structure shared between client and C application
 */

#define NUM_CHANNEL 2

// Default osp user data values

#define D_EN_HA 1
#define D_AFC 0
#define D_REAR_MIC 0
#define D_SAMP_FREQ 48000


/*** Peak detect defaults ***/
#define D_ATTACK_TIME	5	///< Attack time in msec
#define D_RELEASE_TIME	20	///< Release time in msec

/*** WDRC defaults ***/
#define D_G50 0
#define D_G80 0
#define D_KNEE_LOW	45	///< Lower kneepoint in dB SPL. Using the same value for all the bands
#define D_KNEE_HIGH	100	///< Upper kneepoint in dB SPL. Using the same value for all the bands
#define D_MPO 120


#define D_NOISE_ESTIMATION 0
#define D_SPECTRAL_SUB 0
#define D_SPECTRAL_TYPE 0
#define  D_ATTENUATION 0


typedef struct osp_user_data_t {
    int enable_ha = D_EN_HA;					///< No operation.  The audio is passed from input to output in the audio callback
    int afc = D_AFC;					///< AFC Type 0 - None 1 - FXLMS 2 - PMLMS 3 - SLMS
    int rear_mics = D_REAR_MIC;				///< Read mics on/off
    float samp_freq = D_SAMP_FREQ;
    float attenuation_db = D_ATTENUATION;

    // Amplification parameters

    float g50[NUM_BANDS] = {D_G50};			///< The gain values at 50 dB SPL input level
    float g80[NUM_BANDS] = {D_G80};			///< The gain values at 80 dB SPL input level
    float knee_low[NUM_BANDS] = {D_KNEE_LOW};	///< Lower kneepoints for all bands
    float knee_high[NUM_BANDS] = {D_KNEE_HIGH};	///< Upper kneepoints for all bands
    float attack[NUM_BANDS] = {D_ATTACK_TIME};		///< Attack time for WDRC for all bands
    float release[NUM_BANDS] = {D_RELEASE_TIME};		///< Release time for WDRC for all bands
    float mpo = D_MPO;					///< MPO for Max power limit for WDRC


    // Noise management parameters

    int noise_estimation_type = D_NOISE_ESTIMATION; ///< Choose type of Noise estimation technique
    int spectral_type = D_SPECTRAL_TYPE;
    float spectral_subtraction = D_SPECTRAL_SUB; ///< Spectral subtraction Param

    // Adaptive Feedback management parameters

    float mu; ///< Step size parameter
    float rho; ///< Forgetting factor

} osp_user_data;





#endif //OSP_CLION_C_OSP_STRUCTURE_H
